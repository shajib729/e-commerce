import React from "react";
import './Slider.scss'

// Import Swiper React components
import { Swiper, SwiperSlide } from "../../../node_modules/swiper/react";

// Import Swiper styles
import 'swiper/swiper.min.css'
import "swiper/components/pagination/pagination.min.css";

// import Swiper core and required modules
import SwiperCore, { Autoplay,Pagination } from "../../../node_modules/swiper/core";

// install Swiper modules
SwiperCore.use([Autoplay,Pagination]);

function Slider() {
  const sliders = [
    {
      title1:"Trade-In Offer",
      title2:"Super Value Deals",
      title3:"On All Products",
      title4:"Save more with coupons & up to 70% off",
      image: '../images/slider1.png',
      bgImage: "url('../images/slider1.png')",
    },
    {
      title1:"Tech Promotions",
      title2:"Tech Trending",
      title3:"Great Collection",
      title4:"Save more with coupons & up to 20% off",
      image: '../images/slider2.png',
      bgImage: "url('../images/slider2.png')",
    },
    {
      title1:"Upcoming Offer",
      title2:"Big Deals From",
      title3:"Manufacturer",
      title4:"Headphone, Gaming Laptop, PC and more...",
      image: '../images/slider3.png',
      bgImage: "url('../images/slider3.png')",
    }
  ];

  const slider = sliders.map((value, i) => {
    return (
      <SwiperSlide
        key={i}
      >
        <div
        style={{ background:value.bgIamage ,backgroundPosition:"center right",backgroundRepeat:'no-repeat', backgroundSize:"contain", height:"auto" }}
          className="container"
        >
          <div className="row h-100">
            <div className="col-6 d-flex align-items-center">
              <div className="banner_info">
                <div data-aos="fade-right" data-aos-delay="0" data-aos-once="false" data-aos-duration="2000" className="title1">{value.title1}</div>
                <div data-aos="fade-right" data-aos-delay="200" data-aos-once="false" data-aos-duration="2000" className="title2">{value.title2}</div>
                <div data-aos="fade-right" data-aos-delay="400" data-aos-once="false" data-aos-duration="2000" className="title3">{value.title3}</div>
                <div data-aos="fade-right" data-aos-delay="600" data-aos-once="false" data-aos-duration="2000" className="title4">{value.title4}</div>
                <div data-aos="fade-right" data-aos-delay="800" data-aos-once="false" data-aos-duration="2000" data-aos-offset="0" className="shop_now">Shop Now</div>
              </div>
            </div>
            <div className="col-6 text-center right_image">
              <img className="w-100" src={value.image} alt="" data-aos="fade-left" data-aos-once="false" data-aos-duration="2000" />
            </div>
          </div>
        </div>
      </SwiperSlide>
    );
  });

  return (
    <section className="Allslider">
      <Swiper
        // autoplay={{
        //   delay: 10000,
        //   disableOnInteraction: false,
        // }}
        slidesPerView={1}
        spaceBetween={30}
        loop={true}
        grabCursor={true}
        pagination={{
          clickable: true
        }}
        className="mySwiper"
      >
        {slider}
      </Swiper>
    </section>
  );
}

export default Slider;
