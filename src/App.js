import { useEffect } from "react";
import {useLocation} from 'react-router-dom'
import { BrowserRouter, Switch, Route } from "react-router-dom";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import Blog from "./pages/Blog/Blog";
import Footer from './components/Footer/Footer';
import NavBar from './components/NavBar/NavBar'
import Home from './pages/Home/Home'
import Product from "./pages/Product/Product";
import Error from "./components/Error/Error";
import ScrollTop from "./components/ScrollTop/ScrollTop";
import Contact from "./pages/Contact/Contact";
import Blogs from "./pages/Blogs/Blogs";
import About from "./pages/About/About";
import Shop from "./pages/Shop/Shop";
import MobileNavBar from "./components/MobileNavBar/MobileNavBar";
import Login from "./pages/Login/Login";
import SignUp from "./pages/SignUp/SignUp";
import Cart from "./pages/Cart/Cart";
import AOS from 'aos'
import 'aos/dist/aos.css'
import NewsLetter from "./components/NewsLetter/NewsLetter";

const ScrollTop2 = () => {
  const {pathname} = useLocation()

  useEffect(() => {
    window.scrollTo(0, 0)
    
  },[pathname])
  return null
}

function App() {

  useEffect(() => {
    AOS.init()
  },[])

  return (
    <BrowserRouter>
    <div className="App">
      <ScrollTop2/>
      <NavBar />
      <MobileNavBar/>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/blog/:id" component={Blog}/>
          <Route exact path="/product/:id" component={Product}/>
          <Route exact path="/contact" component={Contact}/>
          <Route exact path="/blogs" component={Blogs}/>
          <Route exact path="/shop/:name?" component={Shop}/>
          <Route exact path="/about" component={About}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/signup" component={SignUp}/>
          <Route exact path="/cart" component={Cart}/>
          <Route exact component={Error}/>
        </Switch>
        <NewsLetter />
        <ScrollTop/>
      <Footer/>
    </div>
    </BrowserRouter>
  );
}

export default App;
